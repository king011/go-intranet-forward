package client

import (
	"gitlab.com/king011/go-intranet-forward/protocol"
)

var _MessageHeart protocol.Message

func init() {
	var e error
	_MessageHeart, e = protocol.NewMessage(protocol.Heart, nil)
	if e != nil {
		panic(e)
	}
}
